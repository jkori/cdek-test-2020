package ru.aoklimov.cdek.service;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aoklimov.cdek.domain.CallTask;
import ru.aoklimov.cdek.domain.Order;
import ru.aoklimov.cdek.repo.CallTaskRepo;

@Service
public class CallTaskService {

    private CallTaskRepo repo;

    public CallTaskService(CallTaskRepo repo) {
        this.repo = repo;
    }

    public void createCallTask(Order order, String message) {
        repo.save(new CallTask(order, message));
    }

    public List<CallTask> getCallTasksByDate(Date from, Date to) {
        return repo.getCallTasksByDateBetween(from, to);
    }

    public List<CallTask> getCallTasksByDateAndOrderNumber(Date from, Date to, Long number) {
        return repo.getCallTasksByDateBetween(from, to)
            .stream()
            .filter(callTask -> callTask.getOrder().getId().equals(number))
            .collect(Collectors.toList());
    }

    public Boolean existCallTaskByOrder(Order order) {
        return repo.existsCallTaskByOrder(order);
    }

    @Transactional
    public void deleteCallTaskByOrder(Order order) {
        if (repo.existsCallTaskByOrder(order)) {
            repo.deleteAllByOrder(order);
        }
    }
}
