package ru.aoklimov.cdek.service;

import java.util.Optional;
import org.springframework.stereotype.Service;
import ru.aoklimov.cdek.domain.Order;
import ru.aoklimov.cdek.repo.OrderRepo;

@Service
public class OrderService {

    private OrderRepo repo;

    public OrderService(OrderRepo repo) {
        this.repo = repo;
    }

    public Optional<Order> getById(Long orderId) {
        return repo.findById(orderId);
    }

    public Order saveOrder(Order order) {
        return repo.save(order);
    }

    public void deleteOrder(Order order) {
        repo.deleteById(order.getId());
    }
}
