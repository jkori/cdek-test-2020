package ru.aoklimov.cdek.controller.v1;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aoklimov.cdek.domain.CallTask;
import ru.aoklimov.cdek.domain.Order;
import ru.aoklimov.cdek.service.CallTaskService;
import ru.aoklimov.cdek.service.OrderService;

@RestController
@RequestMapping("/api/v1")
public class CallTaskController {

    private CallTaskService callTaskService;
    private OrderService orderService;

    public CallTaskController(CallTaskService callTaskService, OrderService orderService) {
        this.callTaskService = callTaskService;
        this.orderService = orderService;
    }

    @GetMapping("/notimeorder")
    public Boolean addNoTimeTask(@RequestParam(name = "order") Long orderId) {
        Optional<Order> orderOptional = orderService.getById(orderId);
        if (orderOptional.isPresent() && !callTaskService.existCallTaskByOrder(orderOptional.get())) {
            callTaskService.createCallTask(orderOptional.get(), "No time");
            return true;
        }
        return false;
    }

    @GetMapping("/calltasks")
    public List<CallTask> getCallTask(@NonNull @RequestParam Date from, @NonNull @RequestParam Date to,
        @Nullable @RequestParam(name = "number") Long orderNumber) {
        if (orderNumber != null) {
            return callTaskService.getCallTasksByDateAndOrderNumber(from, to, orderNumber);
        } else {
            return callTaskService.getCallTasksByDate(from, to);
        }
    }
}
