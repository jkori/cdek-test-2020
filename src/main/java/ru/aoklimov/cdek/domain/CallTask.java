package ru.aoklimov.cdek.domain;

import java.sql.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CallTask {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    private Date date;
    @Column(nullable = false)
    private Boolean isDone;
    private String message;
    @OneToOne
    private Order order;

    public CallTask() {
    }

    public CallTask(Order order, String message) {
        this.order = order;
        this.message = message;
        date = new Date(System.currentTimeMillis());
        isDone = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getDone() {
        return isDone;
    }

    public void setDone(Boolean done) {
        isDone = done;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CallTask callTask = (CallTask) o;
        return id.equals(callTask.id) &&
            date.equals(callTask.date) &&
            isDone.equals(callTask.isDone) &&
            Objects.equals(message, callTask.message) &&
            order.equals(callTask.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, isDone, message, order);
    }
}
