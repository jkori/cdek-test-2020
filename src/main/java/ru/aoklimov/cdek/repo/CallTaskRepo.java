package ru.aoklimov.cdek.repo;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.aoklimov.cdek.domain.CallTask;
import ru.aoklimov.cdek.domain.Order;

public interface CallTaskRepo extends JpaRepository<CallTask, Long> {

    List<CallTask> getCallTasksByDateBetween(Date from, Date to);

    Boolean existsCallTaskByOrder(Order order);

    void deleteAllByOrder(Order order);
}
