package ru.aoklimov.cdek.repo;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.aoklimov.cdek.domain.Order;

public interface OrderRepo extends JpaRepository<Order, Long> {

}
