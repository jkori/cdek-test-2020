package ru.aoklimov.cdek;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.aoklimov.cdek.controller.v1.CallTaskController;
import ru.aoklimov.cdek.domain.CallTask;
import ru.aoklimov.cdek.domain.Order;
import ru.aoklimov.cdek.service.CallTaskService;
import ru.aoklimov.cdek.service.OrderService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CdekApplicationTests {

    @Autowired
    private CallTaskController callTaskController;
    @Autowired
    private OrderService orderService;
    @Autowired
    private CallTaskService callTaskService;
    private List<Order> orders;

    @Before
    public void addInfo() {
        orders = new ArrayList<>();
        orders.add(new Order(new Date(System.currentTimeMillis())));
        orders.add(new Order(new Date(System.currentTimeMillis())));
        orders.add(new Order(new Date(System.currentTimeMillis())));
        for (Order order: orders) {
            orderService.saveOrder(order);
        }
    }

    @Test()
    public void trueAddOrder() {
        Assert.assertTrue(callTaskController.addNoTimeTask(orders.get(1).getId()));
    }

    @Test
    public void falseGetCallTask(){
        List<CallTask> callTask = callTaskController
            .getCallTask(orders.get(0).getDate(), orders.get(2).getDate(), orders.get(0).getId());
        Assert.assertEquals(0, callTask.size());
    }

    @Test
    public void falseAddOrder() {
        Order order = orders.get(1);
        orderService.deleteOrder(order);
        orders.remove(order);
        Assert.assertFalse(callTaskController.addNoTimeTask(order.getId()));
    }

    @After
    public void delete() {
        for (Order order : orders) {
            callTaskService.deleteCallTaskByOrder(order);
            orderService.deleteOrder(order);
        }
    }
}
